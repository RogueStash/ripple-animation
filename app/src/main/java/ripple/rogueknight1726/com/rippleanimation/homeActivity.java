package ripple.rogueknight1726.com.rippleanimation;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewDebug;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class homeActivity extends AppCompatActivity {
    ImageView coverPicture,storyImage;
    LinearLayout descriptioncontainer;
    int flag = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        coverPicture = (ImageView)findViewById(R.id.coverPicture);
        coverPicture.setImageBitmap(decodeSampledBitmapFromResource(getResources(),R.drawable.eyes,300,300));
        storyImage = (ImageView)findViewById(R.id.stroyImage);
        storyImage.setImageBitmap(decodeSampledBitmapFromResource(getResources(),R.drawable.water,300,300));





    }
    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }


        public void Fade(View v){

            final View myView = findViewById(R.id.descriptionContainer);
            int cx = myView.getWidth()/2;
            int cy = myView.getHeight()/2;
            Animator anim;

            switch(flag){
                case 0:


                    float finalRadius = (float) Math.hypot(cx, cy);
                    anim =
                            ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, finalRadius);
                    myView.setVisibility(View.VISIBLE);
                    anim.setDuration(300);
                    anim.start();
                    flag = 1;
                    break;
                case 1:
                    float initialRadius = (float) Math.hypot(cx, cy);
                    anim =
                            ViewAnimationUtils.createCircularReveal(myView, cx, cy, initialRadius, 0);
                    anim.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            myView.setVisibility(View.INVISIBLE);
                        }
                    });
                    anim.start();
                    Intent intent = new Intent(this,secondActivity.class);
                    String transitionName = getString(R.string.transitName);
                    ImageView transitionImage=(ImageView)v;

                    ActivityOptionsCompat options =
                            ActivityOptionsCompat.makeSceneTransitionAnimation(homeActivity.this,
                                    transitionImage,   // The view which starts the transition
                                    transitionName    // The transitionName of the view we’re transitioning to
                            );
                    ActivityCompat.startActivity(homeActivity.this, intent, options.toBundle());
                    flag = 0;
                    break;

            }

        }
    public void Transit(View v){
        Intent intent = new Intent(this,secondActivity.class);
        String transitionName = getString(R.string.transitName);
        ImageView transitionImage=(ImageView)v;

        ActivityOptionsCompat options =
                ActivityOptionsCompat.makeSceneTransitionAnimation(homeActivity.this,
                        transitionImage,   // The view which starts the transition
                        transitionName    // The transitionName of the view we’re transitioning to
                );
        ActivityCompat.startActivity(homeActivity.this, intent, options.toBundle());
    }


}
